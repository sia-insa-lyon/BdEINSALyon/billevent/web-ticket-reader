export class File {
  id: number;
  nom: string;
  event: number;
  active: boolean;
  file_parente: File;
  product_type: string;
  option_type: string;
  validation_type: number;

  constructor(nom, event, active, file_parente, validation_type) {
    this.nom = nom;
    this.active = active;
    this.file_parente = file_parente;
    this.validation_type = validation_type;
  }
}
