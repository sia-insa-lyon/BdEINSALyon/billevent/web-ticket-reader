import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ParametresComponent} from './parametres/parametres.component';
import {MainComponent} from './main/main.component';
import {LoginComponent} from './login/login.component';
import {AideComponent} from './aide/aide.component';

const routes: Routes = [
  { path: 'params', component: ParametresComponent},
  {path: '', component: MainComponent},
  {path: 'login', component: LoginComponent},
  {path: 'aide', component: AideComponent}
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})


export class AppRoutingModule {
}
