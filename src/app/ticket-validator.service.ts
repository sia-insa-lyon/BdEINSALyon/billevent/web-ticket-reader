import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Billet} from '../billet';
import {Observable} from 'rxjs/Observable';
import {AppComponent} from './app.component';
import {Place} from './place';
import {File} from './file';
import {environment} from '../environments/environment';

/***Le service qui communique avec l'api par rapport à la validation de billets
 *
  */
@Injectable()
export class TicketValidatorService {

  static server = environment.apiUrl;

  file = 1;

  constructor(private http: HttpClient) {

  }

  /***Renvoie si le billet existe, et si il a été composté.
   *
   * @param {string} id
   * @returns {Observable<Billet>} le billet ou un message d'erreur
   */
  checkBillet(id: string): Observable<Billet> {
    return Observable.create((obs) =>
      this.http.post(TicketValidatorService.server + '/api/billetcheck/', {id: id},
        {headers: new HttpHeaders().set( 'Authorization', 'JWT ' + AppComponent.JWT)}

        ).subscribe(
        data => {
          // Read the result field from the JSON 1:qKlPOxcujQ_KVs60Thy3DAt1X2s
          obs.next(data);
          obs.complete();
        },
        err => {
          obs.error(err);
        }
      ));

  }

  /***Permet de composter le billet
   *
   * @param {Billet} billet
   * @returns {any}
   */
  composterBillet(billet: Billet) {
    return Observable.create((obs) =>
    this.http.post(TicketValidatorService.server + '/api/compostages/',
      {billet: billet.id, file: this.file},
      {headers: new HttpHeaders().set( 'Authorization', 'JWT ' + AppComponent.JWT)}
      ).subscribe(
      data => {
        obs.next(data);
        obs.complete();
      },

      err => obs.error(err)
    )

    );
  }

   getFiles(): Observable<File[]> {
    return Observable.create((obs) =>
    this.http.get(TicketValidatorService.server + '/api/files/',
      {headers: new HttpHeaders().set( 'Authorization', 'JWT ' + AppComponent.JWT)}
    ).subscribe(
      data => {
        obs.next(data);
        obs.complete();
      },
          err => {

            alert('Il n\'existe pas de files ou vous n\'êtes pas gérant de l\'évenement');
          }

    )
    );

  }

  getPlaces(billet: Billet): Observable<Place[]> {
    return Observable.create((obs) =>
      this.http.get(TicketValidatorService.server + '/api/place/' + billet.id,
        {headers: new HttpHeaders().set( 'Authorization', 'JWT ' + AppComponent.JWT)}
      ).subscribe(
        data => {
            const places = Array<Place>();
            for (let i = 0; i < 20; i++) {
              if (data[i] != null) {
                console.log(data[i]);
                places.push(new Place(data[i]));
              }
            }



          obs.next(places);
          obs.complete();
        },

        err => obs.error(err)
      )
    );
  }
  getTable(id: number): Observable<string> {
    return Observable.create((obs) =>
      this.http.get(TicketValidatorService.server + '/api/table/' + id,
        {headers: new HttpHeaders().set( 'Authorization', 'JWT ' + AppComponent.JWT)}
      ).subscribe(
        data => {
          obs.next(data);
          obs.complete();
        },

        err => obs.error(err)
      )
    );
  }

}
