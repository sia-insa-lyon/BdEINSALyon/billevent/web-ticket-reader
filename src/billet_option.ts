export class BilletOption {

  id: number;
  value: number;

  constructor(billet_option) {

    console.log(billet_option);
    this.id = billet_option.id;
    this.value = billet_option.amount;
  }
}
