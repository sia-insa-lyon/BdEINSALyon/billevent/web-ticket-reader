import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  static JWT: string;
  static file: number;
  /***défini si on composte un billet ou si on récupère la place
   *
   */
  static mode_compostage: boolean;

}
